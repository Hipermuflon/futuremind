package com.adamk.futuremindtask.repository

import com.adamk.futuremindtask.domain.client.ClientProvider
import com.adamk.futuremindtask.domain.services.DataService

object DataServiceImpl : DataService {

    override fun getData() = ClientProvider.get().getData()
}
