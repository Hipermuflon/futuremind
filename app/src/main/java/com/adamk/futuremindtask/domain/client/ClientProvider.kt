package com.adamk.futuremindtask.domain.client

import com.adamk.futuremindtask.domain.services.DataService
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ClientProvider {

    fun get(): DataService = client.create(DataService::class.java)

    private val client : Retrofit by lazy{
        Retrofit.Builder()
            .baseUrl("https://www.futuremind.com/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build()).build()
    }
}