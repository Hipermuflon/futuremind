package com.adamk.futuremindtask.domain.model

import com.google.gson.annotations.SerializedName

data class DataApiModel(
    val description: String,
    @SerializedName("image_url")
    val imageUrl: String,
    val modificationDate: String,
    val orderId: Int,
    val title: String
)