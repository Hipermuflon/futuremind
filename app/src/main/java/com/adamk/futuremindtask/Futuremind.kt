package com.adamk.futuremindtask

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class Futuremind : Application() {

    override fun onCreate() {
        super.onCreate()
        initRealm()
    }

    private fun initRealm() {
        Realm.init(this)

        val realmConf = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.setDefaultConfiguration(realmConf)
    }
}