package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.ErrorView
import com.adamk.futuremindtask.common.RefreshableView
import com.adamk.futuremindtask.domain.model.HttpError
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem

class OrderContract {

    interface View : RefreshableView, ErrorView {
        fun displayOrders(orders: List<OrderItem>)
        fun showWebSite(websiteUrl: String)
    }

    interface Presenter {
        fun downloadOrders()
    }
}
