package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.FutureActivity
import com.adamk.futuremindtask.domain.model.HttpError
import com.adamk.futuremindtask.presentation.orders.recycler.OrderAdapter
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.R
import com.adamk.futuremindtask.common.ConnectionManager
import com.adamk.futuremindtask.common.SnackbarFactory
import com.adamk.futuremindtask.common.recycler.HorizontalDivider
import com.adamk.futuremindtask.domain.model.isRetryable
import com.adamk.futuremindtask.presentation.website.WebsiteFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.swipe_refresh.*

class MainActivity : FutureActivity(), OrderContract.View {

    override val layoutResId = R.layout.activity_main
    override val presenter = OrderPresenter(OrderModel(),this)
    private val isTablet:Boolean by lazy{resources.getBoolean(R.bool.isTablet)}

    override fun onViewsBound() {
        orderList.apply {
            addItemDecoration(HorizontalDivider(context))
            adapter = OrderAdapter()
        }
        swipeRefresh.setOnRefreshListener(presenter::downloadOrders)
    }

    override fun displayOrders(orders: List<OrderItem>) {
        setRefreshing(false)
        orderList.adapter = OrderAdapter(orders, ::showWebSite)
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        swipeRefresh.isRefreshing = isRefreshing
    }

    override fun showWebSite(websiteUrl: String) {
        if(ConnectionManager.isConnectedToInternet(this).not()){
            SnackbarFactory.showErrorWithAction(swipeRefresh, HttpError.CONNECTION_ERROR) {showWebSite(websiteUrl)}
            return
        }

        if(isTablet)
            slidingPane.closePane()
        else
            flipper.displayedChild = 1

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.websiteContent,
                WebsiteFragment.newInstance(websiteUrl)
            )
            .commit()
    }

    override fun displayError(error: HttpError) {
        if (error.isRetryable())
            SnackbarFactory.showErrorWithAction(swipeRefresh, error, presenter::downloadOrders)
        else
            SnackbarFactory.showError(swipeRefresh, error)
    }
}
