package com.adamk.futuremindtask.presentation.website

import android.os.Bundle
import android.webkit.WebView
import com.adamk.futuremindtask.common.ErrorView
import com.adamk.futuremindtask.common.FutureFragment
import com.adamk.futuremindtask.common.SnackbarFactory
import com.adamk.futuremindtask.domain.model.HttpError
import com.adamk.futuremindtask.R

class WebsiteFragment : FutureFragment(), ErrorView {

    override val layoutResId = R.layout.website_view
    override val presenter = WebsitePresenter(this)

    override fun onViewsBound() {
        arguments?.getString(URLKEY)?.let{ url ->
            presenter.loadUrl(root as WebView,url)
        }
    }

    override fun displayError(error: HttpError) = SnackbarFactory.showError(root, error)

    companion object {
        private const val URLKEY = "URL"

        fun newInstance(websiteUrl:String) = WebsiteFragment().also {fragment ->
            fragment.arguments = Bundle().also {it.putString(URLKEY,websiteUrl)}
        }
    }
}
