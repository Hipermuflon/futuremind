package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.FutureModel
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.repository.DataServiceImpl
import com.adamk.futuremindtask.repository.OrderDao

open class OrderModel : FutureModel() {

    open fun downloadOrders() =
        DataServiceImpl.getData().async()

    fun readOrders() =
        OrderDao.getData().async()

    open fun saveOrders(orderItems : List<OrderItem>) = OrderDao.saveData(orderItems)
}