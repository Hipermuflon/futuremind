package com.adamk.futuremindtask.presentation.orders

import android.annotation.SuppressLint
import com.adamk.futuremindtask.common.FutureMapper
import com.adamk.futuremindtask.domain.model.DataApiModel
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import io.reactivex.Single
import java.text.SimpleDateFormat
import java.util.*

object OrderItemMapper : FutureMapper<List<DataApiModel>, List<OrderItem>>() {

    override fun map(apiModel: List<DataApiModel>) = with(apiModel) {
        val items = mutableListOf<OrderItem>()

        this.forEach { apiModel ->
            val splittedDesc = apiModel.description.split('\t')
            items += OrderItem(
                description = splittedDesc.tryGet(0).orEmpty(),
                imageUrl = apiModel.imageUrl,
                websiteUrl = splittedDesc.tryGet(1).orEmpty(),
                modificationDate = apiModel.modificationDate.toFormattedDate(),
                orderId = apiModel.orderId,
                title = apiModel.title
            )
        }
        Single.just(items.sortedBy { it.orderId })
    }
}

fun List<String>.tryGet(index: Int) =  if(index > this.size-1) null else this[index]

fun String?.orEmpty() = this ?: ""

@SuppressLint("SimpleDateFormat")
fun String.toFormattedDate() : String{
    val cal = Calendar.getInstance()
    cal.time = SimpleDateFormat("yyyy-MM-dd").parse(this)
    return String.format(
        "%02d.%02d.%02d",
        cal.get(Calendar.DAY_OF_MONTH),
        cal.get(Calendar.MONTH) + 1,
        cal.get(Calendar.YEAR)
    )
}


