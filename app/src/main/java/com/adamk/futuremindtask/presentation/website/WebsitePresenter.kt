package com.adamk.futuremindtask.presentation.website

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Build
import android.util.Log
import android.webkit.*
import com.adamk.futuremindtask.common.ErrorView
import com.adamk.futuremindtask.common.FuturePresenter
import com.adamk.futuremindtask.domain.model.HttpError

class WebsitePresenter(val view: ErrorView) : FuturePresenter() {

    override fun onCreate() = Unit

    private var isWebviewConfigured = false

    @SuppressLint("SetJavaScriptEnabled")
     fun loadUrl(webView: WebView, url: String) {

        webView.run {

            if(isWebviewConfigured.not()) {
                webViewClient = createWebViewClient()
                webChromeClient = WebChromeClient()

                settings.run {
                    javaScriptEnabled = true
                    allowFileAccess = true
                    setAppCacheEnabled(true)
                    cacheMode = WebSettings.LOAD_DEFAULT
                    javaScriptCanOpenWindowsAutomatically = true
                    allowContentAccess = true
                    allowContentAccess = true
                    blockNetworkImage = false
                    pluginState = WebSettings.PluginState.ON
                    domStorageEnabled = true
                    isWebviewConfigured = true
                }
            }
            loadUrl(url)
        }
    }

    private fun createWebViewClient(): WebViewClient = object : WebViewClient() {

        override fun shouldOverrideUrlLoading(webView: WebView, url: String) =  false

        override fun onReceivedError(webView: WebView?, request: WebResourceRequest?, error: WebResourceError) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.displayError(error.errorCode.toAppError())
            }
        }

        override fun onReceivedSslError(webView: WebView?, handler: SslErrorHandler?, error: SslError?) {
            view.displayError(HttpError.UNAUTHORIZED_ERROR)
        }

        override fun onReceivedError(webView: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
            view.displayError(errorCode.toAppError())
        }
    }
}
