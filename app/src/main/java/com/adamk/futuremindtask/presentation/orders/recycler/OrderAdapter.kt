package com.adamk.futuremindtask.presentation.orders.recycler

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.adamk.futuremindtask.common.recycler.FutureAdapter
import com.adamk.futuremindtask.common.recycler.FutureViewHolder
import com.adamk.futuremindtask.common.recycler.inflate
import com.adamk.futuremindtask.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.data_item.view.*

class OrderAdapter(
    val orders: List<OrderItem> = emptyList(),
    val onOrderClickedListener: (webSite:String) -> Unit = {}
) : FutureAdapter(orders) {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = OrderViewHolder(
        inflate(getLayoutResId(),parent)
    )

    override fun getItemCount() = when (orders.size) {
        0 -> 1
        else -> super.getItemCount()
    }

    private fun getLayoutResId() = when (orders.size) {
        0 -> R.layout.empty_view
        else -> R.layout.data_item
    }

    inner class OrderViewHolder(itemView: View) : FutureViewHolder(itemView) {
        override fun bind(position: Int) {
            val order = orders[position]

            itemView.apply {
                setOnClickListener {onOrderClickedListener(order.websiteUrl) }
                title.text = order.title
                description.text = order.description
                date.text = order.modificationDate
                image.loadFromUrl(order.imageUrl)
            }
        }
    }
}

private fun ImageView.loadFromUrl(imageUrl: String) {
    Glide.with(context)
        .load(imageUrl)
        .apply(RequestOptions()
            .error(R.drawable.ic_refresh)
            .placeholder(R.drawable.ic_refresh)
        )
        .into(this)
}
