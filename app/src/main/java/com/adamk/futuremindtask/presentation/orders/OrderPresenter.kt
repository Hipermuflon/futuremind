package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.FuturePresenter
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.repository.OrderRealm
import io.reactivex.Single

open class OrderPresenter(override val model : OrderModel, private val view: OrderContract.View) : OrderContract.Presenter, FuturePresenter(model) {

    override fun onCreate() = downloadOrders()

    override fun downloadOrders() {
        view.setRefreshing(true)
        disposable.add(
            model.downloadOrders()
                .flatMap(OrderItemMapper::map)
                .doOnSuccess(model::saveOrders)
                .subscribe(view::displayOrders)
                { error ->
                    view.displayError(error.toAppError())
                    readOrders()
                }
        )
    }

    private fun readOrders() {
        disposable.add(
            model
            .readOrders()
            .flatMap(::toOrderItems)
            .subscribe( view::displayOrders)
            { error -> view.displayError(error.toAppError())}
        )
    }

    private fun toOrderItems(orderRealm: List<OrderRealm>): Single<List<OrderItem>> {
        val items = mutableListOf<OrderItem>()

        orderRealm.forEach {
            items += OrderItem(
                title = it.title,
                description = it.description,
                orderId = it.orderId,
                modificationDate = it.modificationDate,
                imageUrl = it.imageUrl,
                websiteUrl = it.websiteUrl
            )
        }

        return Single.just(items.toList())
    }

}
