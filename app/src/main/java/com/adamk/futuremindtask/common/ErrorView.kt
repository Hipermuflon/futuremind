package com.adamk.futuremindtask.common

import com.adamk.futuremindtask.domain.model.HttpError

interface ErrorView {
    fun displayError(error: HttpError)
}