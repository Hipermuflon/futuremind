package com.adamk.futuremindtask.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class FutureActivity : AppCompatActivity() {

    abstract val layoutResId: Int
    abstract val presenter: FuturePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        presenter.onCreate()
        onViewsBound()
    }

    abstract fun onViewsBound()

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
