package com.adamk.futuremindtask.common

interface RefreshableView {
    fun setRefreshing(isRefreshing: Boolean)
}
