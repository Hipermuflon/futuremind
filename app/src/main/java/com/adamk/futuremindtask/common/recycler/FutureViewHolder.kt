package com.adamk.futuremindtask.common.recycler

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class FutureViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bind(position:Int)
}
