package com.adamk.futuremindtask.common

import io.reactivex.Single

abstract class FutureMapper<T, U> {

    abstract fun map(value: T): Single<U>
}