package com.adamk.futuremindtask.common

import com.adamk.futuremindtask.domain.model.HttpError
import io.reactivex.disposables.CompositeDisposable

abstract class FuturePresenter(open val model: FutureModel = FutureModel()) {

    val disposable = CompositeDisposable()

    abstract fun onCreate()

    fun onDestroy() {
        if (disposable.isDisposed.not())
            disposable.dispose()
    }

    fun Throwable?.toAppError() = (this?.localizedMessage ?: "").toAppError()

    fun String.toAppError(): HttpError {
        return if (contains("401") || contains("403"))
            HttpError.UNAUTHORIZED_ERROR
        else if (contains("404"))
            HttpError.NOT_FOUND_ERROR
        else if (contains("Unable to resolve host"))
            HttpError.CONNECTION_ERROR
        else if (toLowerCase().contains("time out") ||
            toLowerCase().contains("timed out")
        )
            HttpError.TIMEOUT_ERROR
        else HttpError.UNKNOWN_ERROR
    }

    fun Int.toAppError(): HttpError  = when(this){
       401,403 -> HttpError.UNAUTHORIZED_ERROR
       404     -> HttpError.NOT_FOUND_ERROR
        else   ->HttpError.UNKNOWN_ERROR
    }
}
