package com.adamk.futuremindtask.common.recycler

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class FutureAdapter(val items: List<*>) : RecyclerView.Adapter<FutureViewHolder>() {

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: FutureViewHolder, position: Int){
        if(items.isNotEmpty())
            holder.bind(position)
    }
}

fun inflate(@LayoutRes layoutResId: Int, parent: ViewGroup) =
    LayoutInflater.from(parent.context).inflate(layoutResId,parent,false)
