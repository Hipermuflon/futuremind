package com.adamk.futuremindtask.common

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class FutureFragment: Fragment() {

    lateinit var root: View
    abstract val layoutResId: Int
    abstract val presenter: FuturePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(layoutResId, container, false)
        presenter.onCreate()
        onViewsBound()
        return root
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    abstract fun onViewsBound()
}
