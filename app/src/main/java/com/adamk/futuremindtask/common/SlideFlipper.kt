package com.adamk.futuremindtask.common

import android.content.Context
import android.util.AttributeSet
import android.view.animation.TranslateAnimation
import android.widget.ViewFlipper

class SlideFlipper @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ViewFlipper(context, attrs) {

    override fun setDisplayedChild(child: Int) {
        if (child < displayedChild)
            setLeftInAnimation()
        else
            setRightInAnimation()

        super.setDisplayedChild(child)
    }

    fun goBack(): Boolean {
        return if (displayedChild == 1) {
            displayedChild = 0
            true
        } else false
    }

    private fun setLeftInAnimation() {
        setInAnimation(context, android.R.anim.slide_in_left)
        setOutAnimation(context, android.R.anim.slide_out_right)
    }

    private fun setRightInAnimation() {
        inAnimation = TranslateAnimation( width.toFloat(), 0f, 0f, 0f ).also {
            it.duration = 300
            it.fillAfter = true
        }

        outAnimation = TranslateAnimation( 0f, -width.toFloat(), 0f, 0f ).also {
            it.duration = 300
            it.fillAfter = true
        }
    }
}
