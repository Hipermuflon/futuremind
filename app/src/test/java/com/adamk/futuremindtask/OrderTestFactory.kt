package com.adamk.futuremindtask

import com.adamk.futuremindtask.domain.model.DataApiModel
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem

object OrderTestFactory {
    fun createOrderApiModel(): List<DataApiModel> = listOf(
        DataApiModel(
            "order2" + '\t' + "websiteUrl2",
            "imageUrl2",
            "2012-12-01",
            2,
            "title2"
        ),
        DataApiModel(
            "order1" + '\t' + "websiteUrl1",
            "imageUrl1",
            "2011-11-01",
            1,
            "title1"
        )
    )

    fun createOrderItems() : List<OrderItem> = listOf(
        OrderItem(
            "order1",
            "imageUrl1",
            "websiteUrl1",
            "01.11.2011",
            1,
            "title1"
        ),
        OrderItem(
            "order2",
            "imageUrl2",
            "websiteUrl2",
            "01.12.2012",
            2,
            "title2"
        )
    )
}