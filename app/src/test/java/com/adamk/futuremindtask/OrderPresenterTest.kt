package com.adamk.futuremindtask

import com.adamk.futuremindtask.presentation.orders.OrderContract
import com.adamk.futuremindtask.presentation.orders.OrderModel
import com.adamk.futuremindtask.presentation.orders.OrderPresenter
import com.nhaarman.mockito_kotlin.doNothing
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class OrderPresenterTest {

    private val model: OrderModel = mock()
    private val view: OrderContract.View = mock()
    private val presenter = OrderPresenter(model, view)
    private val testApiModel = OrderTestFactory.createOrderApiModel()
    private val expectedOrderList = OrderTestFactory.createOrderItems()

    @Before
    fun mockRxSchedulers() = RxAndroidPlugins.setInitMainThreadSchedulerHandler{Schedulers.trampoline()}

    @Test
    fun shouldShowOrdersOnBind() {

        // given
        whenever(model.downloadOrders()).thenReturn(Single.just(testApiModel))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        // when
        presenter.onCreate()

        // then
        Mockito.verify(view).displayOrders(expectedOrderList)
    }
}